﻿using System;
using System.Net.Mail;
using System.Web.UI;

namespace eService
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_SendMessage_Click(object sender, EventArgs e)
        {
                string body = null;
                body = "<h4>Correo enviado desde el formulario de contacto eService</h4>";
                body += "<p>Nombre: " + nombre.Text + "</p>";
                body += "<p>Empresa: " + empresa.Text + "</p>";
                body += "<p>Referencia: " + referencia.Text + "</p>";
                body += "<p>Email: " + email.Text + "</p>";
                body += "<p>Telefono: " + telefono.Text + "</p>";
                body += "<p>Poblacion: " + poblacion.Text + "</p>";
                body += "<p>CP: " + cp.Text + "</p>";
                body += "<p>Categoría: " + categoria.SelectedItem.Text + "</p>";
                switch (categoria.SelectedItem.Value)
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                        body += "<p>Observaciones: " + comment.Text + "</p>";
                    break;
                    default:
                        body += "Sin Observaciones</p>";
                    break;
                }
           

            //sendMail(sendTo(categoria.SelectedItem.Value), body);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal(1);", true);
        }

        protected void sendMail(string mailTo, string body)
        {
            SmtpClient smtpClient = new SmtpClient();

            //smtpClient.Credentials = new System.Net.NetworkCredential("info@MyWebsiteDomainName.com", "myIDPassword");
            //smtpClient.UseDefaultCredentials = true;
            //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtpClient.EnableSsl = true;

            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;

            //Setting From , To and CC
            foreach (string item in mailTo.Split(';'))
            {
                mail.To.Add(item);
            }

            //if (adjunto != null)
            //    mail.Attachments.Add(adjunto);

            mail.Subject = My.subjectEmail;
            mail.Body = body;
            //mail.CC.Add(new MailAddress("MyEmailID@gmail.com"));

            smtpClient.Send(mail);
        }

        protected string sendTo(string categoria)
        {
            string mailTo = "";
            switch (categoria)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                    mailTo = My.toOP1;
                    break;
                case "5":
                    mailTo = My.toOP2;
                    break;
                default:
                    mailTo = My.toOP3;
                    break;
            }

            return mailTo;
        }
    }
}