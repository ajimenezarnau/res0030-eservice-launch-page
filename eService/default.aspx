﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="eService._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/custom.css" rel="stylesheet" />
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <title>FormularioContacto</title>
  <script type="text/javascript">
      function showModal(status) {
            $('#myModal').modal('show');
            if (status === 1) {
                var d = document.getElementById('divForm');
                d.style.display = 'none';
                document.getElementById('lblCabecera').innerHTML = 'Mensaje enviado correctamente!';
            }
            
        }

        $(function () {
            $('#btnShowModal').click(function () {
                showModal();
            });
        });
    </script>

</head>
<body>
    <div id="general">
        <div id="cabecera">
            <img src="img/logo.png" />
        </div><div id="form">
            <h3>Solicitud de información</h3>
            <br />
            <h5>Si desea más información en referencia a algún tema en concreto. Puede hacernos llegar dicha solicitud mediante el siguiente formulario.</h5>
            <br /><br />
            <button type="button" class="btn btn-info btn-lg" id="btnShowModal" data-toggle="modal" >Crear solicitud</button>
        </div>
        <div id="body">
            <h3>Centro de Atención al Cliente Online de RICOH España</h3>
            <p class="p_home">La plataforma le permitirá registrar sus peticiones online, de una forma más cómoda, rápida y eficiente.</p>
            <p class="p_home">Si usted ya era Usuario de la anterior plataforma, acceda a la nueva web con el mismo <b>Usuario ID y Contraseña.</b></p>
            <p class="p_home">Si usted es un Nuevo Usuario será imprescindible darse de alta, y registrar los equipos para los que desee realizar cualquier petición.</p>
            <br />
            <table>
                <tr>
                    <td> <img src="img/ricoh-eservice.png" /></td>
                    <td style="padding-left:20px">
                        <p class="p_home"><b>Mayor compatibilidad</b> con los navegadores web.</p>
                        <p class="p_home"><b>Mayor facilidad en el registro</b> de una solicitud de servicio.</p>
                        <p class="p_home">Posibilidad de registrar la <b>lectura de contador para varios equipos a la vez.</b></p>
                        <p class="p_home">Fácil acceso a la <b>“Base de conocimientos”</b>.</p>
                        <p class="p_home">Acceso a <b>"Ricoh Smart Return" (Reciclaje de consumibles usados)</b>.</p>
                    </td>
                </tr>
                <tr><td style="text-align:center;font-size:large;">902 700 000</td></tr>
                <tr><td></td></tr>               
            </table>
        </div>
        <div style="width:100%;margin-bottom:30px;">
            <div id="doc">&nbsp;</div>
            <div id="eservice"><a href="https://eservice.ricoh-europe.com/eservice_esn/start.swe?SWECmd=Login&SWECM=S&SWEHo=eservice.ricoh-europe.com&OID=RES" class="btn btn-info btn-lg">Acceder eService</a></div>
            <div id="doc">
                <img height="175px" src="img/portada.png" /><br />Guia de usuario</div>
        </div>
        
        <div id="footer">
            <p><b>AVISO:</b> Debido a la nueva política de seguridad y protección de datos de RICOH Spain SLU, se deberá actualizar cualquier contraseña que no se haya cambiado durante los últimos 24 meses. Si usted tiene problemas al entrar con su usuario y contraseña por favor siga este <a href="javascript:void(0);" onclick="window.open('reset.html', '_blank', 'toolbar=Not    , scrollbars=Not, resizable=Not, top=300, left=500, width=620, height=340');">link</a> para restablecer su acceso al portal web de Ricoh España</p>
        </div>
    </div>

    

      <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="lblCabecera">Formulario de contacto</h4>
                </div>
                <div class="modal-body" id="divForm">
                    <form id="frmContact" runat="server">
                        <div class="form-group">
                            <label for="usr">Nombre y apellidos *</label>
                            <asp:TextBox ID="nombre" runat="server" class="form-control" />
                            <asp:RequiredFieldValidator id="valTxtName" ControlToValidate="nombre" ErrorMessage="Por favor indique nombre y apellidos" runat="server" ForeColor="Red" />
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-6">
                                <label for="usr">Empresa</label>
                                <asp:TextBox ID="empresa" runat="server" class="form-control" />
                            </div>
                            <div class="col-xs-6">
                                <label for="usr">Número contrato/cliente o factura</label>
                                <asp:TextBox ID="referencia" runat="server" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-6">
                                <label for="email">Email</label>
                                <asp:TextBox Id="email" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-6">
                                <label for="usr">Teléfono</label>
                                <asp:TextBox Id="telefono" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-4">
                                <label for="usr">Código Postal</label>
                                <asp:TextBox Id="cp" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-8">
                                <label for="usr">Población</label>
                                <asp:TextBox Id="poblacion" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Categoría *</label>
                            <asp:DropDownList runat="server" id="categoria" onchange="changecategoria()" class="form-control"> 
                                <asp:ListItem Text="Seleccione la acción a realizar" Value="" Selected="True" />
                                <asp:ListItem Text="Consulta sobre una factura o contrato" Value="1" />
                                <asp:ListItem Text="Solicitud declaración anual de operaciones con terceros (Modelo 347)" Value="2" />
                                <asp:ListItem Text="Otras consultas administrativas" Value="3" />
                                <asp:ListItem Text="Solicitud factura digital" Value="4" />
                                <asp:ListItem Text="Quiero cambiar mis datos (sin cambio de CIF)" Value="5" />
                                <asp:ListItem Text="Quiero subrogar mis contratos (con cambio de CIF)" Value="6" />
                                <asp:ListItem Text="Alta @remote" Value="7" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator id="RequiredFieldValidatorcategoria" ControlToValidate="categoria" ErrorMessage="Por favor seleccione una categoría" runat="server" ForeColor="Red"/>
                        </div>
                        <div class="form-group" id="divcomentarios" style="display:none">
                          <label for="comment">Comentarios</label>
                          <asp:TextBox id="comment" TextMode="multiline" Columns="70" Rows="5" runat="server" class="form-control" />
                          <asp:RequiredFieldValidator id="RequiredFieldValidatorcomment" ControlToValidate="comment" ErrorMessage="Por favor indique los comentarios de la solicitud" runat="server" ForeColor="Red" Enabled="false" />
                        </div>
                        <div class="form-group row" id="divChangeData" style="display:none">
                            <div class="col-xs-6">
                                <label for="usr">Nueva dirección fiscal</label>
                                <asp:TextBox Id="nuevafiscal" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-6">
                                <label for="usr">Nueva dirección de envío facturas</label>
                                <asp:TextBox Id="nuevaenvio" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-xs-12">
                            <label for="sel2">Forma de pago</label>
                            <asp:DropDownList runat="server" id="formapago" class="form-control"> 
                                <asp:ListItem Text="Seleccione una nueva forma de pago" Value="" Selected="True" />
                                <asp:ListItem Text="Domiciliación" Value="1" />
                                <asp:ListItem Text="Otras formas de pago" Value="2" />
                            </asp:DropDownList>
                            </div>
                            <div class="col-xs-12">
                                <p>Indique el número de los contratos afectados o en su defecto números de serie:</p>
                                <asp:RadioButtonList ID="rdafectados" runat="server" onchange="changelist(this)">
                                    <asp:ListItem Text="Todos" Value="0" Selected = "True"/>
                                    <asp:ListItem Text="Solo algunos (indicar) " Value="1" />
                                </asp:RadioButtonList>
                                <asp:TextBox Id="serialafectados" runat="server" class="form-control" disabled="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group" id="divRemote" style="display:none">
                          <label for="comment">Para gestionar esta petición acceda a <a href="https://monitor.ricoh.es/cumin" target="_top">Alta @remote</a>.</label>
                        </div>
                        <div class="form-group" id="divEnviar" style="display:block">
                            <asp:Button runat="server" ID="btnSend" Text="Enviar" class="btn btn-lg btn-primary btn-block" OnClick="btn_SendMessage_Click"/>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-muted">
                                    <strong>*</strong> Campos requeridos.
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

        <script>

        $('.modal').on('hidden.bs.modal', function () {
            var d = document.getElementById('divForm');
            if (d.style.display === 'none') {
                d.style.display = 'block';
                document.getElementById('lblCabecera').innerHTML = 'Formulario de contacto';
            }
            
            $(this).find('form')[0].reset();
        });

        function changelist(radioButtonList) {
            var selected;
            for (var i = 0; i < radioButtonList.rows.length; ++i) {
                if (radioButtonList.rows[i].cells[0].firstChild.checked) {
                    selected = radioButtonList.rows[i].cells[0].firstChild.value;
                }
            }
            if (selected === '1') {
                document.getElementById('<%= serialafectados.ClientID %>').disabled = false;
            } else {
                document.getElementById('<%= serialafectados.ClientID %>').disabled = true;
            }

        }

        function changecategoria() {
            var x = document.getElementById('categoria').value;
            if (x == 1 || x == 2 || x == 3 || x == 4)
            {
                var d = document.getElementById('divcomentarios');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                    document.getElementById("comment").focus();
                    ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorcomment.ClientID %>'), true);
                }

                var d = document.getElementById('divEnviar');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                }
                var d = document.getElementById('divRemote');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
                var d = document.getElementById('divChangeData');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
            } else if (x == 5) {
                var d = document.getElementById('divChangeData');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                    document.getElementById("nuevafiscal").focus();
                }
                var d = document.getElementById('divcomentarios');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                    ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorcomment.ClientID %>'), false);
                }
                var d = document.getElementById('divEnviar');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                }
                var d = document.getElementById('divRemote');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
            } else if (x == 7) {
                var d = document.getElementById('divRemote');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                }
                var d = document.getElementById('divcomentarios');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                    ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorcomment.ClientID %>'), false);
                }
                var d = document.getElementById('divEnviar');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
                var d = document.getElementById('divChangeData');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
            } else if (x == '') {
                var d = document.getElementById('divcomentarios');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                    ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorcomment.ClientID %>'), false);
                }
                var d = document.getElementById('divEnviar');
                if (d.style.display === 'none') {
                    d.style.display = 'block';
                }
                var d = document.getElementById('divRemote');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
                var d = document.getElementById('divChangeData');
                if (d.style.display === 'block') {
                    d.style.display = 'none';
                }
            }
            
        }
    </script>

</body>
</html>
