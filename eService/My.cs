﻿using System.Configuration;

namespace eService
{
    public class My
    {
        public static string subjectEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["subjectEmail"];
            }
        }

        public static string toOP1
        {
            get
            {
                return ConfigurationManager.AppSettings["toOP1"];
            }
        }

        public static string toOP2
        {
            get
            {
                return ConfigurationManager.AppSettings["toOP2"];
            }
        }

        public static string toOP3
        {
            get
            {
                return ConfigurationManager.AppSettings["toOP3"];
            }
        }
    }
}